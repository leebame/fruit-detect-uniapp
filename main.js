
// #ifndef VUE3
import Vue from 'vue'
import App from './App'
Vue.config.productionTip = false
// 自封装的request
// 按需导入 $http 对象

import {
     $request
} from '@/utils/request.js'
//  $request 挂载到 uni 顶级对象之上，全局调用
// $request.baseUrl = 'http://localhost:8081'
$request.baseUrl = 'http://101.43.149.52:8081'
uni.$request =  $request
//请求拦截器
$request.beforeRequest = function(options) {
	uni.showLoading({
		title: '数据加载中...'
	})
	return options
}
// 响应拦截器
$request.afterRequest = function(res) {
	uni.hideLoading()
	return res
}

App.mpType = 'app'

const app = new Vue({
    ...App
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
import App from './App.vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif